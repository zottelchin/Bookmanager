package Bookmanager

import (
	"github.com/gin-gonic/gin"
)

func listBooks(c *gin.Context) {
	c.JSON(200, loadBooks())
}

func newBook(c *gin.Context) {
	if !authenticate(c) {
		c.Status(401)
		return
	}
	b := Book{}
	c.BindJSON(&b)
	if saveBook(b) {
		c.Status(200)
		return
	}
	c.Status(500)
}

func newLocation(c *gin.Context) {
	if !authenticate(c) {
		c.Status(401)
		return
	}
	l := Location{}
	c.BindJSON(&l)
	if saveLocation(l) {
		c.Status(200)
		return
	}
	c.Status(500)
}

func listLocations(c *gin.Context) {
	c.JSON(200, loadLocations())
}