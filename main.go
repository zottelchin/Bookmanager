package Bookmanager

import (
	"os"

	"codeberg.org/momar/logg"

	"github.com/gin-gonic/gin"
	parcelServe "github.com/moqmar/parcel-serve"
)

func Start() {
	_, passSet := os.LookupEnv("Pass")
	if !passSet {
		logg.Error("Password not set")
		os.Exit(3)
	}
	router := createRoutes()
	parcelServe.Serve("frontend", router, AssetNames(), MustAsset)
	router.Run(":2407")
}

func createRoutes() *gin.Engine {
	r := gin.Default()
	api := r.Group("/api/v1")
	api.GET("/books", listBooks)
	api.POST("/books", newBook)
	api.GET("/shelfs", listLocations)
	api.POST("/shelfs", newLocation)

	api.GET("/version", func(c *gin.Context) {
		c.JSON(418, gin.H{
			"version": version,
			"build":   buildDate,
			"commit":  gitHash,
		})
	})
	return r
}

func authenticate(c *gin.Context) bool {
	key := c.GetHeader("X-Auth")
	return key == os.Getenv("Pass")
}
