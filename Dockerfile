FROM node AS Frontend
ADD ./frontend /frontend
WORKDIR /frontend
RUN npm install -g parcel-bundler
RUN npm install && \
    parcel build index.html

FROM golang AS Backend
ADD .git /go/src/codeberg.org/zottelchin/Bookmanager/
ADD *.go /go/src/codeberg.org/zottelchin/Bookmanager/
ADD cmd /go/src/codeberg.org/zottelchin/Bookmanager/cmd/
COPY --from=Frontend /frontend/dist /go/src/codeberg.org/zottelchin/Bookmanager/frontend/dist
WORKDIR /go/src/codeberg.org/zottelchin/Bookmanager
RUN echo 'package Bookmanager \n\nvar gitHash = "'$(git rev-parse HEAD)'" \nvar buildDate = "'$(TZ=":Europe/Berlin" date '+%d.%m.%Y %H:%M %Z')'" \nvar version = "'$(git describe --tags)'"' > version.go
RUN go get github.com/go-bindata/go-bindata/... && \
    /go/bin/go-bindata -pkg Bookmanager ./frontend/dist/... && \
    go get -u -v
RUN CGO_ENABLED=1 GOOS=linux go build -a -ldflags '-extldflags "-static" -s' -installsuffix cgo -o bookmanager -v ./cmd/cmd.go

FROM scratch
COPY --from=Backend /go/src/codeberg.org/zottelchin/Bookmanager/bookmanager /bookmanager
ADD ./sql /sql
ENV GIN_MODE=release
WORKDIR /
EXPOSE 2407
VOLUME [ "/data" ]

ENTRYPOINT [ "/bookmanager" ]