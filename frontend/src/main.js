import "babel-polyfill";
import Vue from 'vue';
import App from './App.vue';

import RestAPI from './rest-api';

window.api = new RestAPI("/api/v1", { headers: { "X-Auth": localStorage.pass ? localStorage.pass : "" } });

new Vue({
    el: '#app',
    render: h => h(App)
});