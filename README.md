# Bookmanager
Bookmanager ist ein Webtool um die eigenen Bücher zu verwalten

# Installation
docker-compose.yml
```yml
version: "3"
services:
    web:
        image: zottelchin/bookmanager:latest
        environment:
            Pass= #Hier das Passwort eintragen, mit dem Änderungen vorgenommen werden können. Passwortgenrator pag.momar.io
        ports:
            "2407:2407"
```
wenn der Container gestartet ist, sollte der Sever unter locahost:2407 erreichbar sein

# geplante Features
- System ausweiten, sodass Nutzer eigene Profile haben könne
- Verleih und Leih Übersicht