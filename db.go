package Bookmanager

import (
	"database/sql"
	"os"

	_ "github.com/mattn/go-sqlite3"

	"codeberg.org/momar/logg"
	"github.com/pressly/goose"
)

var db = initDB()

func initDB() *sql.DB {
	goose.SetDialect("sqlite3")
	db, err := sql.Open("sqlite3", "./data/storage.db")
	if err != nil {
		panic(err)
	}
	if db == nil {
		panic("db is nil")
	}

	if err := goose.Up(db, "./sql"); err != nil {
		logg.Error("Error in Database migration: %s", err)
		os.Exit(1)
	}

	return db
}

func saveBook(book Book) bool {
	_, err := db.Exec("Insert into Books (title, author, isbn, publisher, location, coverURL) values (?, ?, ?, ?, ?, ?)", book.Title, book.Author, book.ISBN, book.Publisher, book.Location, book.CoverURL)
	if err != nil {
		return false
	}
	return true
}

func loadBooks() []Book {
	books := []Book{}
	rows, _ := db.Query("Select id, title, author, isbn, publisher, location, coverURL from Books")
	defer rows.Close()
	for rows.Next() {
		tmp := Book{}
		rows.Scan(&tmp.ID, &tmp.Title, &tmp.Author, &tmp.ISBN, &tmp.Publisher, &tmp.Location, &tmp.CoverURL)
		books = append(books, tmp)
	}
	return books
}

func saveLocation(l Location) bool {
	_, err := db.Exec("Insert into Locations (name, comment) values (?, ?)", l.Name, l.Comment)
	if err != nil {
		logg.Error("Error saving Location: %s", err)
		return false
	}
	return true
}

func loadLocations() []Location {
	shelfs := []Location{}
	rows, _ := db.Query("Select id, name, comment from Locations")
	defer rows.Close()
	for rows.Next() {
		tmp := Location{}
		rows.Scan(&tmp.ID, &tmp.Name, &tmp.Comment)
		shelfs = append(shelfs, tmp)
	}
	return shelfs
}

type Book struct {
	ID        int
	Title     string
	Author    string
	ISBN      string
	Publisher string
	Location  int
	CoverURL  string
}

type Location struct {
	ID      int
	Name    string
	Comment string
}
